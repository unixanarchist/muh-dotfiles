# muh dotfiles

Some dotfiles for totally serious configuration & ricing

## Installation
If you're a Linux newbie, install git and
```
git clone  https://gitlab.com/unixanarchist/muh-dotfiles
```
Then copy these files to your .config or your home or your etc. Where exactly? Read the manual. Or the Arch wiki. Or Gentoo wiki. Or Linux from Scratch.
Enjoy.

## Contributing
You're welcome! Just don't break anything.
